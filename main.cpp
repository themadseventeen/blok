#include<bits/stdc++.h>

using namespace std;

struct Point {
    double x, y;

    Point(int _x, int _y) {
        x = _x;
        y = _y;
    }

    Point() {
        x = y = 0;
    }

    bool operator < (const Point &other) const {
        if (x == other.x)
            return y < other.y;
        return x < other.x;
    }

    double distance(const Point &other) const {
        return sqrt((x - other.x) * (x - other.x) + (y - other.y) * (y - other.y));
    }

    friend std::ostream& operator << (std::ostream &strm, const Point &P) {
        return strm << "Point=[" << P.x << ", " << P.y << "]";
    }

    friend void operator >> (std::istream &strm, Point &P) {
        strm >> P.x >> P.y;
    }
};

double Area(std::vector<Point> v) {

    double minY = DBL_MAX;
    for(Point x : v) {
        minY = min(minY, x.y);
    }

    if(minY < 0) {
        for(auto it = v.begin() ; it != v.end() ; it++) {
            it -> y -= minY;
        }
    }

    //nadjemo tacke sa najmanjim i najvecim x kordinatama
    auto it = std::minmax_element(v.begin(), v.end(), [](Point a, Point b){
        return a.x < b.x;
    });
    auto it1 = it.first, it2 = it.second;
    auto i = it1;

    //podelimo omotac u gornji i donji deo

    std::vector<Point> upper_hull, lower_hull;

    while(i != it2) {
        upper_hull.emplace_back(*i);
        i++;
        if(i == v.end()) i = v.begin();
    }
    upper_hull.emplace_back(*i);
    i = it2;
    while(i != it1) {
        lower_hull.emplace_back(*i);
        i++;
        if(i == v.end()) i = v.begin();
    }
    lower_hull.emplace_back(*i);

    // for(auto x : upper_hull) std::cout << x << " ";
    // std::cout << "\n";
    // for(auto x : lower_hull) std::cout << x << " ";
    // std::cout << "\n";

    double ret = 0;

    //racunamo povrsine trapeza gornjeg i donjeg omotaca

    int size = upper_hull.size();
    for(int i = 0 ; i < size - 1 ; i++) {
        double m = ((abs(upper_hull[i].y) + abs(upper_hull[i + 1].y)) / 2.0);
        ret += (abs(upper_hull[i].x - upper_hull[i + 1].x) * m);
    }

    size = lower_hull.size();

    for(int i = 0 ; i < size - 1 ; i++) {
        double m = ((abs(lower_hull[i].y) + abs(lower_hull[i + 1].y)) / 2.0);
        ret -= (abs(lower_hull[i].x - lower_hull[i + 1].x) * m);
    }

    return ret;
}

int main () {
    int n;
    std::cin >> n;
    std::vector<Point> v(n);
    for(int i = 0 ; i < n ; i++) std::cin >> v.at(i);

    double p = Area(v);
    std::cout << p << "\n";
    return 0;
}